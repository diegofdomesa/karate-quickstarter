function fn(){
	var config = {
		name : 'morpheus',
		baseUrl : 'https://reqres.in/api',
		testText: 'DEFAULT'
	}
	
	var env = karate.env
	karate.log('the value of env is: ',env)
	
	if (env == 'qa'){
		config.testText = 'QA'
	}
	else if (env == 'prod'){
		config.testText = 'PROD'
	}
	else {
		config.testText = 'LOCAL'
	}	
	
	return config
}