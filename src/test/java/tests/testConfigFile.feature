Feature: POST API Demo

  Background: 
    * url baseUrl
    * header Accept = 'application/json'
    * def filesPath = '../data/'
    * def expectedOutput = read(filesPath+'response1.json')         

  Scenario: Post Demo 1 using config variables and  passing variables from the prevous job
    Given path '/users'
    And request {"name": "#(name)","job": "leader"}
    When method POST
    Then status 201
    And match $.name == name
    And print response
        
    Given path '/users/'+response.id
    When method GET
    Then print response
    
	Scenario: Check config properties per env
		Given print name
		And print testText    