Feature: POST API Demo

  Background: 
    * url 'https://reqres.in'
    * header Accept = 'application/json'
    * def filesPath = '../data/'
    * def expectedOutput = read(filesPath+'response1.json')         

  Scenario: Post Demo 1
    Given path '/api/users'
    And request {"name": "morpheus","job": "leader"}
    When method POST
    Then status 201
    And print response

  Scenario: Post Demo 2
    Given path '/api/users'
    And request {"name": "morpheus","job": "leader"}
    When method POST
    Then status 201
    And match $.name == "morpheus"
    
  Scenario: Post Demo 3
    Given path '/api/users'
    And request {"name": "morpheus","job": "leader"}
    When method POST
    Then status 201
    And match response == {"name":"morpheus","job":"leader","id":"#string","createdAt":"#ignore"}
    
  Scenario: Post Demo 4
    Given path '/api/users'
    And request {"name": "morpheus","job": "leader"}
    When method POST
    Then status 201
    And print response
    And match response == expectedOutput
    
  Scenario: Post Demo 5
    Given path '/api/users'
    And def requestBody = read(filesPath+'request1.json')
    And print requestBody
    And request requestBody
    When method POST
    Then status 201
    And print response
    And match response == expectedOutput  